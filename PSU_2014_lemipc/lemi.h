/*
** lemi.h for lemi in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri Feb 27 15:38:50 2015 Clement Da Silva
** Last update Thu Mar  5 18:50:49 2015 Clement Da Silva
*/

#ifndef LEMI_H_
# define LEMI_H_

# define NB_X		5
# define NB_Y		5
# define NB_TEAM	2
# define NB_PLAYER	9

# define SIZEX		800
# define SIZEY		600

# define KEY		123654

# include <sys/ipc.h>

typedef struct	s_msgbuf
{
  long		mtype;
  char		str[512];
}		t_msgbuf;

typedef struct	s_player
{
  int		x;
  int		y;
  int		team;
  int		nbr;
  char		dead;
}		t_player;

typedef struct	s_id
{
  int		shm_id;
  int		sem_id;
  int		msg_id;
}		t_id;

t_player	*get_player(int, void *);
void		draw_board(void *, int);
void		create_player(int, void *);
void		check_board(void *, int);
char		is_dead(int, void *);
char		game_end(void *);
void		*spawn(t_player *, int, void *, int);
void		admin_turn(t_player *, void *, int);
void		user_turn(int, t_msgbuf *, int, void *);
void		xexit(int);
void		kill_player(t_player *, t_msgbuf *, int, void *);
int		**create_tab();
int		check_validity();
int		is_in(char *, char);
int		playing(int, void *, int);
int		make_move(t_player *, void *);
int		check_end(t_player *, void *);
int		board(int, int, void *, int);
int		init_shm(key_t, int *, void **);
int		kill_remaining(void *, int, t_msgbuf *);
int		init_sem(key_t);
int		init_msg(key_t);
int		die(t_player *, void *, int, t_msgbuf *);
int		arg_init(int, char **);
int		stop(int, int, int);
int		init_ipc(t_id *, int, int *, void **);
int		start(int, void *, t_id *);
int		playing_user(int, void *, int, int);
int		playing_admin(int, void *, int, int);
void		draw_initial_state(void *, int *, char *, int);

#endif /* !LEMI_H_ */
