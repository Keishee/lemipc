/*
** init.c for lemi in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Mon Mar  2 20:11:44 2015 Clement Da Silva
** Last update Fri Mar  6 15:17:07 2015 Clement Da Silva
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "lemi.h"

void		create_player(int pnbr, void *addr)
{
  t_player	p;

  p.x = pnbr;
  p.y = pnbr;
  p.team = pnbr;
  p.nbr = pnbr;
  p.dead = 0;
  addr = ((char *)addr + ((NB_X * NB_Y) + 1) * sizeof(int));
  addr = ((char *)addr + ((pnbr - 1) * sizeof(t_player)));
  memcpy(addr, &p, sizeof(p));
}

int		init_shm(key_t key, int *pnbr, void **addr)
{
  int		shm_id;

  shm_id = shmget(key, (NB_X * NB_Y * sizeof(int) +
			(NB_PLAYER * sizeof(t_player) + 10)),
		  SHM_R | SHM_W);
  if (shm_id == -1)
    {
      shm_id = shmget(key, (NB_X * NB_Y * sizeof(int) +
			    (NB_PLAYER * sizeof(t_player) + 10)),
		      IPC_CREAT | SHM_R | SHM_W);
      if (shm_id == -1 || *(int *)(*addr = shmat(shm_id,
						 NULL, SHM_R | SHM_W)) == -1)
	xexit(-1);
      *(int *)(*addr) = 1;
      *pnbr = 1;
    }
  else
    {
      if (*(int *)(*addr = shmat(shm_id, NULL, SHM_R | SHM_W)) == -1)
	xexit(-1);
      *((int *)(*addr)) += 1;
      *pnbr = *(int *)(*addr);
    }
  create_player(*pnbr, *addr);
  return (shm_id);
}

int		init_sem(key_t key)
{
  int		sem_id;

  sem_id = semget(key, 1, SHM_R | SHM_W);
  if (sem_id == -1)
    {
      sem_id = semget(key, 1, IPC_CREAT | SHM_R | SHM_W);
      if (sem_id == -1)
	xexit(-1);
      if (semctl(sem_id, 0, SETVAL, 0) == -1)
	xexit(-1);
    }
  return (sem_id);
}

int		init_msg(key_t key)
{
  int		msg_id;

  if ((msg_id = msgget(key, SHM_R | SHM_W)) == -1)
    {
      msg_id = msgget(key, IPC_CREAT | SHM_R | SHM_W);
      if (msg_id == -1)
	xexit(-1);
    }
  return (msg_id);
}

int		stop(int shm_id, int sem_id, int msg_id)
{
  shmctl(shm_id, IPC_RMID, NULL);
  msgctl(msg_id, IPC_RMID, NULL);
  semctl(sem_id, 0, IPC_RMID);
  return (0);
}
