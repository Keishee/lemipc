/*
** sdl.c for ds in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc/display
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Mar  5 11:24:48 2015 Clement Da Silva
** Last update Fri Mar  6 15:16:22 2015 Clement Da Silva
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <time.h>
#include "../lemi.h"
#include "display.h"

int		init_msg_sdl(key_t key)
{
  int		msg_id;

  if ((msg_id = msgget(key, SHM_R | SHM_W)) == -1)
    {
      msg_id = msgget(key, IPC_CREAT | SHM_R | SHM_W);
    }
  return (msg_id);
}

int		init_shm_sdl(key_t key)
{
  int		shm_id;

  shm_id = shmget(key, (NB_X * NB_Y * sizeof(int) +
			(NB_PLAYER * sizeof(t_player) + 10)),
		  SHM_R);
  if (shm_id == -1)
    {
      fprintf(stderr, "LemiPC should be launched first\n");
      exit(1);
    }
  return (shm_id);
}

int	board_sdl(int x, int y, void *addr)
{
  if (x < 0 || y < 0 || x >= NB_X || y >= NB_Y)
    return (-1);
  addr = ((int *)addr) + 1;
  return *((int *)addr + (x * NB_X + y));
}

void		get_message(SDL_Surface *screen)
{
  int		msg_id;
  int		shm_id;
  void		*addr;
  SDL_Event	evt;
  SDL_Rect	rect;
  t_msgbuf	buf;
  char		end;

  msg_id = init_msg_sdl(KEY);
  shm_id = init_shm_sdl(KEY);
  addr = shmat(shm_id, NULL, SHM_R | SHM_W);
  buf.mtype = 5000;
  end = 0;
  rect.w = SIZEX / NB_X;
  rect.h = SIZEY / NB_Y;
  while (!end)
    {
      SDL_PollEvent(&evt);
      if (evt.key.keysym.sym == SDLK_ESCAPE)
	end = 1;
      msgrcv(msg_id, &buf, 42, buf.mtype, 0);
      draw_board_sdl(screen, addr, rect);
    }
}

int		main()
{
  SDL_Surface	*screen;

  srand(time(NULL));
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
      fprintf(stderr, "SDL Error\n");
      return (0);
    }
  screen = SDL_SetVideoMode(SIZEX, SIZEY, 8, SDL_SWSURFACE);
  if (screen == NULL)
    {
      fprintf(stderr, "SDL Error\n");
      return (0);
    }
  atexit(SDL_Quit);
  get_message(screen);
  return (0);
}
