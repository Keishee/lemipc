/*
** drawing.c for draw in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc/display
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Mar  5 16:55:15 2015 Clement Da Silva
** Last update Thu Mar  5 17:08:22 2015 Clement Da Silva
*/

#include <unistd.h>
#include <SDL/SDL.h>
#include <time.h>
#include "../lemi.h"
#include "display.h"

Uint32		get_random_color(SDL_Surface *screen, char c)
{
  Uint32	color;
  Uint8		r;
  Uint8		g;
  Uint8		b;

  r = (c * 8888) % 255;
  g = (c * 8888) % 255;
  b = (c * 8888) % 255;
  color = SDL_MapRGB(screen->format, r, g, b);
  return (color);
}

void		draw_board_sdl(SDL_Surface *screen, void *addr, SDL_Rect rect)
{
  int		x;
  int		y;
  Uint32	color;

  x = 0;
  y = 0;
  SDL_FillRect(screen, NULL, 0x000000);
  while (x < NB_X)
    {
      y = 0;
      rect.x = (SIZEX / NB_X) * x;
      while (y < NB_Y)
	{
	  rect.y = (SIZEY / NB_Y) * y;
	  if (board_sdl(x, y, addr) != 0)
	    {
	      color = get_random_color(screen, board_sdl(x, y, addr));
	      SDL_FillRect(screen, &rect, color);
	    }
	  ++y;
	}
      ++x;
    }
  SDL_Flip(screen);
}
