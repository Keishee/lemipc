/*
** display.h for fkd in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc/display
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Thu Mar  5 16:58:56 2015 Clement Da Silva
** Last update Thu Mar  5 17:10:17 2015 Clement Da Silva
*/

#ifndef DISPLAY_H_
# define DISPLAY_H_

#include <SDL/SDL.h>

int		board_sdl(int, int, void *);
void		draw_board_sdl(SDL_Surface *, void *, SDL_Rect );

#endif /* !DISPLAY_H_ */
