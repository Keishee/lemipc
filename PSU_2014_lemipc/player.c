/*
** player.c for lemipc in /home/barbis_j/Documents/Projets/lemipc/PSU_2014_lemipc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Mar  4 15:11:15 2015 Joseph Barbisan
** Last update Thu Mar  5 16:37:39 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "lemi.h"

void		*spawn(t_player *player, int pnbr, void *addr, int sem_id)
{
  struct sembuf	sops;

  player->nbr = pnbr;
  player->team = pnbr % NB_TEAM + 1;
  player->x = rand() % NB_X;
  player->y = rand() % NB_Y;
  while (board(player->x, player->y, addr, -1) != 0)
    {
      player->x = rand() % NB_X;
      player->y = rand() % NB_Y;
    }
  board(player->x, player->y, addr, player->team);
  sops.sem_num = 0;
  sops.sem_flg = 0;
  sops.sem_op = 1;
  if (semop(sem_id, &sops, 1) == -1)
    xexit(-1);
  return (player);
}

int		die(t_player *player, void *addr, int msg_id, t_msgbuf *buf)
{
  board(player->x, player->y, addr, 0);
  *(int *)addr -= 1;
  player->dead = 1;
  printf("%d player(s) remaining\n", *(int *)addr);
  buf->mtype = 1;
  if (msgsnd(msg_id, buf, 512, 0) == -1)
    xexit(-1);
  return (0);
}

void		admin_turn(t_player *player, void *addr,
			   int msg_id)
{
  make_move(player, addr);
  check_board(addr, msg_id);
  draw_board(addr, msg_id);
  sleep(1);
}

void		user_turn(int player_nbr, t_msgbuf *buf,
			  int msg_id, void *addr)
{
  buf->mtype = player_nbr;
  if (msgsnd(msg_id, buf, 512, 0) == -1)
    xexit(-1);
  buf->mtype = 1;
  if (msgrcv(msg_id, buf, 512, buf->mtype, 0) == -1)
    xexit(-1);
  check_board(addr, msg_id);
  draw_board(addr, msg_id);
  sleep(1);
}
