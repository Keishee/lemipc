/*
** end2.c for end in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed Mar  4 18:49:56 2015 Clement Da Silva
** Last update Thu Mar  5 18:34:58 2015 Clement Da Silva
*/

#include <stdlib.h>
#include <stdio.h>
#include "lemi.h"

void	xexit(int nb)
{
  perror("Error: ");
  exit(nb);
}

char	game_end(void *addr)
{
  char	tab[NB_TEAM + 1];
  char	c;
  int	x;
  int	y;
  int	i;

  i = 0;
  x = 0;
  tab[NB_TEAM + 1] = 0;
  while (x <= NB_TEAM + 1)
    tab[x++] = -1;
  x = 0;
  while (x != NB_X)
    {
      y = 0;
      while (y != NB_Y)
	{
	  c = board(x, y, addr, -1);
	  if (!is_in(tab, c))
	    tab[i++] = board(x, y, addr, -1);
	  ++y;
	}
      ++x;
    }
  return ((char)(i == 2 ? 1 : 0));
}

int		kill_remaining(void *addr, int msg_id, t_msgbuf *buf)
{
  int		i;
  t_player	*p;

  i = 1;
  while (i <= NB_PLAYER)
    {
      p = get_player(i, addr);
      if (!p->dead)
	kill_player(p, buf, msg_id, addr);
      ++i;
    }
  return (1);
}

int	check_validity()
{
  if (NB_X < 2 || NB_Y < 2 || NB_X > 200 || NB_Y > 200)
    {
      fprintf(stderr, "Incorrect board (min 2/2 max 200/200)\n");
      return (-1);
    }
  if (NB_PLAYER >= NB_X * NB_Y - 15)
    {
      fprintf(stderr, "Too many players for that board\n");
      return (-1);
    }
  if (NB_PLAYER < NB_TEAM * 2)
    {
      fprintf(stderr, "Not enough players to fill the teams\n");
      return (-1);
    }
  return (1);
}
