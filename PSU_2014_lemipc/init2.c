/*
** init2.c for lemipc in /home/barbis_j/Documents/Projets/lemipc/PSU_2014_lemipc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Wed Mar  4 17:37:19 2015 Joseph Barbisan
** Last update Fri Mar  6 14:56:44 2015 Clement Da Silva
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "lemi.h"

int		init_ipc(t_id *ids, int key, int *pnbr, void **addr)
{
  ids->shm_id = init_shm(key, pnbr, addr);
  ids->msg_id = init_msg(key);
  ids->sem_id = init_sem(key);
  return (0);
}

int		start(int pnbr, void *addr, t_id *ids)
{
  if (pnbr != 1)
    {
      playing_user(pnbr, addr, ids->sem_id, ids->msg_id);
    }
  else
    {
      playing_admin(pnbr, addr, ids->sem_id, ids->msg_id);
      return (stop(ids->shm_id, ids->sem_id, ids->msg_id));
    }
  return (0);
}

void		draw_initial_state(void *addr, int *count,
				   char *end, int msg_id)
{
  *count = 1;
  *end = 0;
  printf("Game starts in 5 seconds ...\n");
  sleep(5);
  draw_board(addr, msg_id);
}
