/*
** main.c for lemipc in /home/barbis_j/Documents/Projets/lemipc/PSU_2014_lemipc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Feb 26 16:46:29 2015 Joseph Barbisan
** Last update Sun Mar  8 19:08:59 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "lemi.h"

t_player	*get_player(int pnbr, void *addr)
{
  t_player	*p;

  addr = ((char *)addr + ((NB_X * NB_Y) + 1) * sizeof(int));
  addr = ((char *)addr + ((pnbr -1) * sizeof(t_player)));
  p = (t_player *)(addr);
  return (p);
}

char		is_dead(int pnbr, void *addr)
{
  t_player	*p;

  p = get_player(pnbr, addr);
  return (p->dead);
}

int		playing_user(int pnbr, void *addr, int sem_id, int msg_id)
{
  t_player	*player;
  int		count;
  t_msgbuf	buf;

  player = get_player(pnbr, addr);
  spawn(player, pnbr, addr, sem_id);
  count = 1;
  while (1)
    {
      buf.mtype = pnbr;
      msgrcv(msg_id, &buf, 512, buf.mtype, 0);
      if (is_dead(pnbr, addr) == 1)
	return (die(player, addr, msg_id, &buf));
      make_move(player, addr);
      ++count;
      buf.mtype = 1;
      msgsnd(msg_id, &buf, 512, 0);
    }
  return (0);
}

int		playing_admin(int pnbr, void *addr, int sem_id, int msg_id)
{
  t_player	*player;
  t_msgbuf	buf;
  int		count;
  int		player_nbr;
  char		end;

  player = get_player(pnbr, addr);
  spawn(player, pnbr, addr, sem_id);
  draw_initial_state(addr, &count, &end, msg_id);
  while (!end)
    {
      player_nbr = count % NB_PLAYER + 1;
      if (!is_dead(player_nbr, addr))
	{
	  if (player_nbr == 1)
	    admin_turn(player, addr, msg_id);
	  else
	    user_turn(player_nbr, &buf, msg_id, addr);
	}
      ++count;
      end = game_end(addr);
    }
  return (kill_remaining(addr, msg_id, &buf));
}

int		main(int ac, char **av)
{
  key_t		key;
  t_id		ids;
  int		pnbr;
  void		*addr;
  struct sembuf	sops;

  if (check_validity() == -1)
    return (1);
  key = KEY;
  init_ipc(&ids, key, &pnbr, &addr);
  srand(time(NULL));
  if ((ac == 3 && strcmp(av[2], "stop") == 0) ||
      (ac == 2 && strcmp(av[1], "stop") == 0))
    return (stop(ids.shm_id, ids.sem_id, ids.msg_id));
  if (pnbr > NB_PLAYER)
    return (0);
  if (pnbr != NB_PLAYER)
    {
      sops.sem_num = 0;
      sops.sem_flg = 0;
      sops.sem_op = -1;
      if (semop(ids.sem_id, &sops, 1) == -1)
	return (-1);
    }
  return (start(pnbr, addr, &ids));
}
