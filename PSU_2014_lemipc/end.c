/*
** end.c for end in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Wed Mar  4 16:20:21 2015 Clement Da Silva
** Last update Thu Mar  5 17:04:51 2015 Clement Da Silva
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lemi.h"

char		**fill_tab(char **way)
{
  way[0][0] = -1;
  way[0][1] = -1;
  way[1][0] = -1;
  way[1][1] = 0;
  way[2][0] = -1;
  way[2][1] = 1;
  way[3][0] = 0;
  way[3][1] = -1;
  way[4][0] = 0;
  way[4][1] = 1;
  way[5][0] = 1;
  way[5][1] = -1;
  way[6][0] = 1;
  way[6][1] = 0;
  way[7][0] = 1;
  way[7][1] = 1;
  return (way);
}

char		**create_way_tab()
{
  char		**way;
  int		i;

  i = 0;
  if ((way = malloc(8 * sizeof(char *))) == NULL)
    return (NULL);
  while (i < 8)
    {
      if ((way[i] = malloc(2 * sizeof(char))) == NULL)
	return (NULL);
      ++i;
    }
  if (way)
    way = fill_tab(way);
  return (way);
}

int	is_in(char *tab, char c)
{
  int	i;

  i = 0;
  while (i < NB_TEAM + 1)
    {
      if (tab[i] == c)
	return (1);
      ++i;
    }
  return (0);
}

int	check_end_team(t_player *player, int team, void *addr, char **way)
{
  int	i;
  int	tmpx;
  int	tmpy;
  int	count;

  count = 0;
  i = 0;
  if (player->team != team)
    while (i < 8)
      {
	tmpx = player->x + way[i][0];
	tmpy = player->y + way[i][1];
	if (tmpx >= 0 && tmpx < NB_X && tmpy >= 0 && tmpy < NB_Y &&
	    board(tmpx, tmpy, addr, -1) == team)
	  ++count;
	++i;
      }
  return (count >= 2 ? 1 : 0);
}

int		check_end(t_player *player, void *addr)
{
  int		count;
  int		i;
  char		**way;

  if ((way = create_way_tab()) == NULL)
    return (1);
  count = 0;
  i = 1;
  while (i <= NB_TEAM)
    {
      count += check_end_team(player, i, addr, way);
      ++i;
    }
  return ((count >= 1 ? 1 : 0));
}
