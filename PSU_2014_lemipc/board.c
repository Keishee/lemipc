/*
** board.c for lemipc in /home/da-sil_l/Depos/lemipc/PSU_2014_lemipc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Mon Mar  2 20:14:28 2015 Clement Da Silva
** Last update Thu Mar  5 18:19:25 2015 Joseph Barbisan
*/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "lemi.h"

void		draw_board(void *addr, int msg_id)
{
  int		i;
  void		*new_addr;
  t_msgbuf	buf;

  buf.mtype = 5000;
  if (msgsnd(msg_id, &buf, 42, 0) == -1)
    xexit(-1);
  system("clear");
  new_addr = ((int *)addr + 1);
  i = 0;
  while (i < NB_X * NB_Y)
    {
      printf("|");
      if (*((int *)new_addr + i) != 0)
	printf("\033[36m%d\033[00m", *((int *)new_addr + i));
      else
	printf(".");
      ++i;
      if (i % NB_X == 0)
	printf("|\n");
    }
}

int		make_move(t_player *player, void *addr)
{
  int		new_x;
  int		new_y;

  new_x = rand() % 3 - 1;
  new_y = rand() % 3 - 1;
  while (board(player->x + new_x, player->y + new_y, addr, -1) != 0)
    {
      new_x = rand() % 3 - 1;
      new_y = rand() % 3 - 1;
    }
  board(player->x, player->y, addr, 0);
  player->x += new_x;
  player->y += new_y;
  player->x = (player->x == -1 ? 0 : player->x);
  player->y = (player->y == -1 ? 0 : player->y);
  player->x = (player->x == NB_X ? NB_X - 1 : player->x);
  player->y = (player->y == NB_Y ? NB_Y - 1 : player->y);
  board(player->x, player->y, addr, player->team);
  return (0);
}

void		kill_player(t_player *player, t_msgbuf *buf,
			    int msg_id, void *addr)
{
  player->dead = 1;
  if ((buf->mtype = player->nbr) != 1)
    {
      if (msgsnd(msg_id, buf, 512, 0) == -1)
	xexit(-1);
      buf->mtype = 1;
      if (msgrcv(msg_id, buf, 512, 1, 0) == -1)
	xexit(-1);
    }
  else
    {
      board(player->x, player->y, addr, 0);
      *(int *)addr -= 1;
      printf("%d player(s) remaining\n", *(int *)addr);
    }
}

void		check_board(void *addr, int msg_id)
{
  t_player	*player;
  int		nb;
  t_msgbuf	buf;

  nb = 1;
  while (nb <= NB_PLAYER)
    {
      if (is_dead(nb, addr) != 1)
	{
	  player = get_player(nb, addr);
	  if (check_end(player, addr) == 1)
	    kill_player(player, &buf, msg_id, addr);
	}
      ++nb;
    }
}

int		board(int x, int y, void *addr, int flag)
{
  if (x < 0 || y < 0 || x >= NB_X || y >= NB_Y)
    return (-1);
  addr = ((int *)addr) + 1;
  if (flag < 0)
    return (*((int *)addr + (y * NB_X + x)));
  *((int *)addr + (y * NB_X + x)) = flag;
  return (0);
}
